package pompei.maths.difur.physic001.ball;

import pompei.maths.difur.physic001.movie.Frame;

public interface Figure {
  void paint(Frame frame);
}
