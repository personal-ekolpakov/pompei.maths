package pompei.maths.difur.physic001.movie;

import java.nio.file.Path;

public class Movie {

  public final int width;
  public final int height;
  private final double x1;
  private final double y1;
  private final double x2;
  private final double y2;
  private final Path targetDir;

  private int nextFrameIndex = 0;

  public Movie(int width, int height, double x1, double y1, double x2, double y2, Path targetDir) {
    this.width = width;
    this.height = height;
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.targetDir = targetDir;
  }

  public Frame createFrame() {
    int index = nextFrameIndex++;
    String I = "" + index;
    while (I.length() < 5) {
      I = "0" + I;
    }
    Path outFile = targetDir.resolve("img" + I + ".png");
    return new Frame(outFile, width, height, x1,y1,x2,y2);
  }
}
