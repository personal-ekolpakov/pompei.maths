package pompei.maths.difur.physic001.ball;

public interface Dot {
  double getX();

  double getY();

  double getVX();

  double getVY();

  void addForceSource(ForceSource forceSource);
}
