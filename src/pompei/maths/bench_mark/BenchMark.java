package pompei.maths.bench_mark;

import java.util.List;

import static pompei.maths.utils.Conv.doubleToStr;
import static pompei.maths.utils.Conv.nanoToSecDouble;
import static pompei.maths.utils.Conv.sizeBeforePoint;

public class BenchMark {

  public static void main(String[] args) {
    new BenchMark().execute();
  }

  private void execute() {

    var javaKeys = List.of("java.runtime.name",
                           "java.version",
                           "java.specification.version",
                           "java.specification.vendor",
                           "java.runtime.version",
                           "java.vm.name");

    for (String javaKey : javaKeys) {
      var value = System.getProperty(javaKey);
      System.out.println("riFr5498BR :: " + javaKey + " -> " + value);
    }

    System.out.println();
    System.out.println();
    System.out.println();

    System.out.println("UB2BI8Us0U :: Прогреваем...");
    for (int top = 10; top < 20; top++) {
      FibLong.execAndGetNanos(16, top);
      FibDouble.execAndGetNanos(16, top);
    }

    System.out.println("Kl4u1V1BG6 :: Считаем...");
    System.out.println();

    final int threadCount = 16;

    long begin = System.nanoTime(), prevNano = begin;

    System.out.println(
        "              N          time, s            time, s            deltaTime, s       totalTime, s");
    System.out.println("                         fibLong(N)         fibDouble(N)");

    System.out.println();

    for (int top = 30; top <= 48; top++) {
      final int N = top <= 40 ? 5 : 1;

      double secLongSum = 0;
      for (int i = 0; i < N; i++) {
        secLongSum += nanoToSecDouble(FibLong.execAndGetNanos(threadCount, top));
      }
      double secLong = secLongSum / (double) N;

      double secDoubleSum = 0;
      for (int i = 0; i < N; i++) {
        secDoubleSum += nanoToSecDouble(FibDouble.execAndGetNanos(threadCount, top));
      }
      double secDouble = secDoubleSum / (double) N;

      long currentNano = System.nanoTime();
      String displayStepTime = "";
      if (prevNano == 0) {
        prevNano = currentNano;
      } else {
        displayStepTime = doubleToStr(nanoToSecDouble(currentNano - prevNano), 8);
        prevNano = currentNano;
      }

      String total = doubleToStr(nanoToSecDouble(currentNano - begin), 8);

      int sizeBeforePoint = 4;

      System.out.println("B3526xD1XE :: " + top
                             + "      " + sizeBeforePoint(doubleToStr(secLong, 8), sizeBeforePoint)
                             + "      " + sizeBeforePoint(doubleToStr(secDouble, 8), sizeBeforePoint)
                             + "      " + sizeBeforePoint(displayStepTime, sizeBeforePoint)
                             + "      " + sizeBeforePoint(total, sizeBeforePoint)
                        );
    }

  }

}
