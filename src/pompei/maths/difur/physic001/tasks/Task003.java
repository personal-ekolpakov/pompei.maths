package pompei.maths.difur.physic001.tasks;

import pompei.maths.difur.Stepper_H5_RungeKutta;
import pompei.maths.difur.physic001.ball.AirFriction;
import pompei.maths.difur.physic001.ball.Ball;
import pompei.maths.difur.physic001.ball.Fix;
import pompei.maths.difur.physic001.ball.Rubber;
import pompei.maths.difur.physic001.ball.Task;
import pompei.maths.difur.physic001.movie.Movie;

import java.nio.file.Paths;

public class Task003 {
  public static void main(String[] args) {

    Fix fix = new Fix();
    fix.x = 7;
    fix.y = 10;

    Ball ball = new Ball();
    ball.x = 13;
    ball.y = 3;
    ball.mas = 3;
    ball.radius = 0.5;

    Ball ball2 = new Ball();
    ball2.x = 17;
    ball2.y = 3;
    ball2.mas = 1;
    ball2.radius = 0.2;

    AirFriction airFriction = new AirFriction();
    airFriction.connect(ball);
    AirFriction airFriction2 = new AirFriction();
    airFriction2.connect(ball2);

    airFriction.k1 = 0.1;
    airFriction.k2 = 0;
    airFriction2.k1 = 0.1;
    airFriction2.k2 = 0;


    Rubber rubber = new Rubber();
    rubber.K = 700;
    rubber.L = 5;

    Rubber rubber2 = new Rubber();
    rubber2.K = 40;
    rubber2.L = 1;

    rubber.connect1(fix);
    rubber.connect2(ball);
    rubber2.connect1(ball);
    rubber2.connect2(ball2);


    Task task = new Task();
    task.registerBall(ball);
    task.registerBall(ball2);
    task.registerFigure(fix);
    task.registerFigure(rubber);
    task.registerFigure(rubber2);

    task.movie = new Movie(800, 600, 0, 0, 20, 15, Paths.get("build/task003/png_list"));

    task.t1 = 0;
    task.t2 = 80;
    task.dt = 0.0001;

    task.stepper = new Stepper_H5_RungeKutta();

    task.go();
  }
}
