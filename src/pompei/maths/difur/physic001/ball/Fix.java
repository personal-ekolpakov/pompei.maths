package pompei.maths.difur.physic001.ball;

import pompei.maths.difur.physic001.movie.Frame;

import java.awt.Color;

public class Fix implements Figure, Dot {

  public double x, y;

  @Override
  public void paint(Frame frame) {
    frame.setColor(new Color(0, 0, 0));

    int T = 5;
    int H = 7;
    int u = 4;

    frame.lineIntBased(x, y, 0, 0, -T, -H);
    frame.lineIntBased(x, y, 0, 0, +T, -H);

    frame.lineIntBased(x, y, -T - u, -H, +T + u, -H);

    frame.drawIntCircleBased(x, y, 0, 0, 3);
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  @Override
  public double getVX() {
    return 0;
  }

  @Override
  public double getVY() {
    return 0;
  }

  @Override
  public void addForceSource(ForceSource forceSource) {
    // It is not moving
  }
}
