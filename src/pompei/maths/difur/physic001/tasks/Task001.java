package pompei.maths.difur.physic001.tasks;

import pompei.maths.difur.F;
import pompei.maths.difur.Stepper;
import pompei.maths.difur.Stepper_H5_RungeKutta;
import pompei.maths.difur.physic001.tasks.task001.Ball001;
import pompei.maths.difur.physic001.tasks.task001.Rubber001;
import pompei.maths.difur.physic001.movie.Frame;
import pompei.maths.difur.physic001.movie.Movie;
import pompei.maths.utils.Vec2;

import java.awt.Color;
import java.nio.file.Paths;

public class Task001 {

  public static void main(String[] args) {
    Rubber001 rubber = new Rubber001();
    Ball001 ball = new Ball001();
    ball.mas = 3;

    rubber.x1 = () -> 10;
    rubber.y1 = () -> 10;
    rubber.x2 = () -> ball.x;
    rubber.y2 = () -> ball.y;
    rubber.L = 4;
    rubber.K = 400;

    F F = (res, t, x) -> {
      ball.x = x[0];
      ball.y = x[1];

      Vec2 rubberForce2 = rubber.Force2();

      res[0] = x[2];
      res[1] = x[3];
      res[2] = rubberForce2.x / ball.mas;
      res[3] = rubberForce2.y / ball.mas - 9.8;
    };

    Movie movie = new Movie(800, 600, 0, 0, 16, 12, Paths.get("build/task001/png_list"));

    Stepper stepper = new Stepper_H5_RungeKutta();

    stepper.prepare(4);
    {
      double[] x = stepper.getX();
      x[0] = 10.5;
      x[1] = 4.5;
      x[2] = 0;
      x[3] = 0;
    }

    double t1 = 0, t2 = 20, dt = 0.0001;

    double nextFrameT = 0;

    for (double t = t1; t <= t2; t += dt) {

      if (t >= nextFrameT) {
        nextFrameT += 1.0 / 24;

        double[] x = stepper.getX();
        ball.x = x[0];
        ball.y = x[1];

        try (Frame frame = movie.createFrame()) {
          ball.paint(frame);
          rubber.paint(frame);

          frame.setColor(new Color(0, 0, 0));
          frame.drawText(400, 50, "t = " + t);
          frame.drawText(400, 60, "ball.x = " + ball.x);
          frame.drawText(400, 70, "ball.y = " + ball.y);

        }

      }

      stepper.step(F, t, dt, 4);

    }

  }

}
