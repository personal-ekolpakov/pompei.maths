package pompei.maths.difur.physic001.ball;

import pompei.maths.utils.Vec2;

public interface ForceSource {
  Vec2 force();
}
