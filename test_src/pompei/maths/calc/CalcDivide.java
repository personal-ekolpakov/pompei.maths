package pompei.maths.calc;

import lombok.SneakyThrows;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class CalcDivide {

  public static void main(String[] args) {
    new CalcDivide().exec();
  }

  private PrintWriter out;

  @SneakyThrows
  private void exec() {
    try (PrintWriter out = new PrintWriter("build/_data_.txt")) {
      this.out = out;
      exec0();
    }
  }

  private void exec0() {

    MathContext mc = new MathContext(100, RoundingMode.HALF_EVEN);

    BigDecimal one = BigDecimal.ONE;
    BigDecimal three = BigDecimal.ONE.add(BigDecimal.ONE).add(BigDecimal.ONE);

    BigDecimal oneOverThree = one.divide(three, mc);

    out.println("14bCMkA8VZ :: " + oneOverThree);

    BigDecimal sqrtOneOverThree = oneOverThree.sqrt(mc);

    out.println("017MYVDjDT :: " + sqrtOneOverThree);

    BigDecimal current = sqrtOneOverThree;

    BigInteger max = BigInteger.ZERO;
    int index = -1;

    for (int i = 1; i <= 20000; i++) {

      out.println("L8ql3Gm3GQ :: i     =       " + i);
      BigDecimal under = BigDecimal.ONE.divide(current, mc);

      out.println("146C12X7wm :: under =       " + under);

      BigInteger U = under.toBigInteger();

      if (max.compareTo(U) < 0) {
        max = U;
        index = i;
      }

      out.println("41Dw9xi8em :: U     = " + U);
      BigDecimal dr = under.subtract(new BigDecimal(U));
      out.println("C95djF595P :: dr    =       " + dr);

      current = dr;
    }

    out.println("EDx2zISDcM :: index = " + index);
    out.println("tzUzEyDlN8 :: max   = " + max);

  }
}
