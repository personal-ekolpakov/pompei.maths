package pompei.maths.difur.physic001.ball;

import pompei.maths.difur.physic001.movie.Frame;
import pompei.maths.utils.Vec2;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Ball implements Figure, Dot {
  public double mas, radius;
  public double x, y;
  public double vx, vy;

  public int index;

  @Override
  public void paint(Frame frame) {
    frame.setColor(new Color(117, 31, 6));
    frame.fillCircle(x, y, radius);
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  @Override
  public double getVX() {
    return vx;
  }

  @Override
  public double getVY() {
    return vy;
  }

  private final List<ForceSource> forceSourceList = new ArrayList<>();

  @Override
  public void addForceSource(ForceSource forceSource) {
    forceSourceList.add(forceSource);
  }

  public Vec2 Force() {
    Vec2 ret = Vec2.xy(0, 0);

    for (ForceSource forceSource : forceSourceList) {
      ret = ret.plus(forceSource.force());
    }

    return ret;
  }
}
