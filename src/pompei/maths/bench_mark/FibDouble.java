package pompei.maths.bench_mark;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;

public class FibDouble extends Thread {

  private final int top;
  public double result;

  public FibDouble(int top) {
    this.top = top;
  }

  @SneakyThrows
  public static long execAndGetNanos(int threadCount, int top) {
    List<FibDouble> threads = new ArrayList<>();

    for (int i = 0; i < threadCount; i++) {
      threads.add(new FibDouble(top));
    }

    var startedAt = System.nanoTime();
    for (FibDouble t : threads) {
      t.start();
    }
    for (FibDouble t : threads) {
      t.join();
    }
    var finishedAt = System.nanoTime();

    return finishedAt - startedAt;
  }

  @Override
  public void run() {
    result = fib(top);
  }

  private static double fib(double n) {
    return n <= 1 ? 1.01 : fib(n - 1) + fib(n - 2);
  }
}
