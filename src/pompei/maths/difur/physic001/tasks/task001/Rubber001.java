package pompei.maths.difur.physic001.tasks.task001;

import pompei.maths.difur.physic001.movie.Frame;
import pompei.maths.utils.Vec2;

import java.awt.Color;
import java.util.function.DoubleSupplier;

public class Rubber001 {
  public DoubleSupplier x1, y1;
  public DoubleSupplier x2, y2;
  public double K, L;

  public Vec2 Force1() {

    double X1 = x1.getAsDouble();
    double Y1 = y1.getAsDouble();
    double X2 = x2.getAsDouble();
    double Y2 = y2.getAsDouble();

    double dX = X2 - X1;
    double dY = Y2 - Y1;

    double Length = Math.sqrt(dX * dX + dY * dY);

    double Delta = Length - L;

    if (Delta <= 0) {
      return Vec2.xy(0, 0);
    }

    double Force = Delta * K;

    return Vec2.xy(dX, dY).div(Length).mul(Force);
  }

  private boolean isShort() {
    double X1 = x1.getAsDouble();
    double Y1 = y1.getAsDouble();
    double X2 = x2.getAsDouble();
    double Y2 = y2.getAsDouble();

    double dX = X2 - X1;
    double dY = Y2 - Y1;

    double Length = Math.sqrt(dX * dX + dY * dY);

    double Delta = Length - L;

    return Delta <= 0;
  }

  public Vec2 Force2() {
    return Force1().mul(-1);
  }

  public void paint(Frame frame) {

    Color colorShort = new Color(63, 108, 54);
    Color colorLong = new Color(1, 1, 1);

    frame.setColor(isShort() ? colorShort : colorLong);

    double X1 = x1.getAsDouble();
    double Y1 = y1.getAsDouble();
    double X2 = x2.getAsDouble();
    double Y2 = y2.getAsDouble();

    frame.line(X1, Y1, X2, Y2);
  }
}
