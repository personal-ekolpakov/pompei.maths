package pompei.maths.bench_mark;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;

public class FibLong extends Thread {

  private final int top;
  public long result;

  public FibLong(int top) {
    this.top = top;
  }

  @SneakyThrows
  public static long execAndGetNanos(int threadCount, int top) {
    List<FibLong> threads = new ArrayList<>();

    for (int i = 0; i < threadCount; i++) {
      threads.add(new FibLong(top));
    }

    var startedAt = System.nanoTime();
    for (FibLong t : threads) {
      t.start();
    }
    for (FibLong t : threads) {
      t.join();
    }
    var finishedAt = System.nanoTime();

    return finishedAt - startedAt;
  }

  @Override
  public void run() {
    result = fib(top);
  }

  private static long fib(long n) {
    return n <= 1 ? 1 : fib(n - 1) + fib(n - 2);
  }
}
