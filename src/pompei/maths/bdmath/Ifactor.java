package pompei.maths.bdmath;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

/**
 * Factored integers.
 * This class contains a non-negative integer with the prime factor decomposition attached.
 *
 * @author Richard J. Mathar
 * @since 2006-08-14
 * @since 2012-02-14 The internal representation contains the bases, and becomes sparser if few
 * prime factors are present.
 */
public class Ifactor implements Cloneable, Comparable<Ifactor> {
  /**
   * The standard representation of the number
   */
  public BigInteger n;

  /*
   * The bases and powers of the prime factorization.
   * representation n = primeExp[0]^primeExp[1]*primeExp[2]^primeExp[3]*...
   * The value 0 is represented by an empty vector, the value 1 by a vector of length 1
   * with a single power of 0.
   */
  public Vector<Integer> primeExp;

  final public static Ifactor ONE = new Ifactor(1);

  final public static Ifactor ZERO = new Ifactor(0);

  /**
   * Constructor given an integer.
   * constructor with an ordinary integer
   *
   * @param number the standard representation of the integer
   * @author Richard J. Mathar
   */
  public Ifactor(int number) {
    n = new BigInteger("" + number);
    primeExp = new Vector<>();
    if (number > 1) {
      int primeIndex = 0;
      Prime primes = new Prime();
      /* Test division against all primes.
       */
      while (number > 1) {
        int ex = 0;
        /* primeIndex=0 refers to 2, =1 to 3, =2 to 5, =3 to 7 etc
         */
        int p = primes.at(primeIndex).intValue();
        while (number % p == 0) {
          ex++;
          number /= p;
          if (number == 1) {
            break;
          }
        }
        if (ex > 0) {
          primeExp.add(p);
          primeExp.add(ex);
        }
        primeIndex++;
      }
    } else if (number == 1) {
      primeExp.add(1);
      primeExp.add(0);
    }
  } /* Ifactor */

  /**
   * Constructor given a BigInteger .
   * Constructor with an ordinary integer, calling a prime factor decomposition.
   *
   * @param number the BigInteger representation of the integer
   * @author Richard J. Mathar
   */
  public Ifactor(BigInteger number) {
    n = number;
    primeExp = new Vector<>();
    if (number.compareTo(BigInteger.ONE) == 0) {
      primeExp.add(1);
      primeExp.add(0);
    } else {
      int priMinDx = 0;
      Prime primes = new Prime();
      /* Test for division against all primes.
       */
      while (number.compareTo(BigInteger.ONE) > 0) {
        int ex = 0;
        BigInteger p = primes.at(priMinDx);
        while (number.remainder(p).compareTo(BigInteger.ZERO) == 0) {
          ex++;
          number = number.divide(p);
          if (number.compareTo(BigInteger.ONE) == 0) {
            break;
          }
        }
        if (ex > 0) {
          primeExp.add(p.intValue());
          primeExp.add(ex);
        }
        priMinDx++;
      }
    }
  } /* Ifactor */

  /**
   * Constructor given a list of exponents of the prime factor decomposition.
   *
   * @param powers the vector with the sorted list of exponents.
   *               powers[0] is the exponent of 2, powers[1] the exponent of 3, powers[2] the exponent of 5 etc.
   *               Note that this list does not include the primes, but assumes a continuous prime-smooth basis.
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public Ifactor(Vector<Integer> powers) {
    primeExp = new Vector<>(2 * powers.size());
    if (powers.size() > 0) {
      n = BigInteger.ONE;
      Prime primes = new Prime();
      /* Build the full number by the product of all powers of the primes.
       */
      for (int primeIndex = 0; primeIndex < powers.size(); primeIndex++) {
        int ex = powers.elementAt(primeIndex);
        final BigInteger p = primes.at(primeIndex);
        n = n.multiply(p.pow(ex));
        primeExp.add(p.intValue());
        primeExp.add(ex);
      }
    } else {
      n = BigInteger.ZERO;
    }
  } /* Ifactor */

  /**
   * Copy constructor.
   *
   * @param oth the value to be copied
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public Ifactor(Ifactor oth) {
    n = oth.n;
    primeExp = oth.primeExp;
  } /* Ifactor */

  /**
   * Deep copy.
   *
   * @author Richard J. Mathar
   * @since 2009-08-14
   */
  @SuppressWarnings({"MethodDoesntCallSuperMethod"})
  public Ifactor clone() {
    Ifactor cl = new Ifactor(0);
    cl.n = new BigInteger("" + n);
    return cl;
  } /* Ifactor.clone */

  /**
   * Comparison of two numbers.
   * The value of this method is in allowing the Vector.contains() calls that use the value,
   * not the reference for comparison.
   *
   * @param oth the number to compare this with.
   * @return true if both are the same numbers, false otherwise.
   * @author Richard J. Mathar
   */
  public boolean equals(final Ifactor oth) {
    return (n.compareTo(oth.n) == 0);
  } /* Ifactor.equals */

  /**
   * Multiply with another positive integer.
   *
   * @param oth the second factor.
   * @return the product of both numbers.
   * @author Richard J. Mathar
   */
  public Ifactor multiply(final BigInteger oth) {
    /* the optimization is to factorize oth _before_ multiplying
     */
    return (multiply(new Ifactor(oth)));
  } /* Ifactor.multiply */

  /**
   * Multiply with another positive integer.
   *
   * @param oth the second factor.
   * @return the product of both numbers.
   * @author Richard J. Mathar
   */
  public Ifactor multiply(final int oth) {
    /* the optimization is to factorize oth _before_ multiplying
     */
    return (multiply(new Ifactor(oth)));
  } /* Ifactor.multiply */

  /**
   * Multiply with another positive integer.
   *
   * @param oth the second factor.
   * @return the product of both numbers.
   * @author Richard J. Mathar
   */
  public Ifactor multiply(final Ifactor oth) {
    /* This might be done similar to the lcm() implementation by adding
     * the powers of the components and calling the constructor with the
     * list of exponents. This here is the simplest implementation, but slow because
     * it calls another prime factorization of the product:
     * return( new Ifactor(n.multiply(oth.n))) ;
     */
    return multiGcdLcm(oth, 0);
  }

  /**
   * Lowest common multiple of this with oth.
   *
   * @param oth the second parameter of lcm(this,oth)
   * @return the lowest common multiple of both numbers. Returns zero
   * if any of both arguments is zero.
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public Ifactor lcm(final Ifactor oth) {
    return multiGcdLcm(oth, 2);
  }

  /**
   * Greatest common divisor of this and oth.
   *
   * @param oth the second parameter of gcd(this,oth)
   * @return the lowest common multiple of both numbers. Returns zero
   * if any of both arguments is zero.
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public Ifactor gcd(final Ifactor oth) {
    return multiGcdLcm(oth, 1);
  }

  /**
   * Multiply with another positive integer.
   *
   * @param oth  the second factor.
   * @param type 0 to multiply, 1 for gcd, 2 for lcm
   * @return the product, gcd or lcm of both numbers.
   * @author Richard J. Mathar
   */
  protected Ifactor multiGcdLcm(final Ifactor oth, int type) {
    Ifactor prod = new Ifactor(0);
    /* skip the case where 0*something =0, falling through to the empty representation for 0
     */
    if (primeExp.size() != 0 && oth.primeExp.size() != 0) {
      /* Cases of 1 times something return something.
       * Cases of lcm(1, something) return something.
       * Cases of gcd(1, something) return 1.
       */
      if (primeExp.firstElement() == 1 && type == 0) {
        return oth;
      } else if (primeExp.firstElement() == 1 && type == 2) {
        return oth;
      } else if (primeExp.firstElement() == 1 && type == 1) {
        return this;
      } else if (oth.primeExp.firstElement() == 1 && type == 0) {
        return this;
      } else if (oth.primeExp.firstElement() == 1 && type == 2) {
        return this;
      } else if (oth.primeExp.firstElement() == 1 && type == 1) {
        return oth;
      } else {
        int idxThis = 0;
        int idxOth = 0;
        switch (type) {
          case 0 -> prod.n = n.multiply(oth.n);
          case 1 -> prod.n = n.gcd(oth.n);
          case 2 ->
            /* the awkward way, lcm = product divided by gcd
             */
              prod.n = n.multiply(oth.n).divide(n.gcd(oth.n));
        }

        /* scan both representations left to right, increasing prime powers
         */
        while (idxOth < oth.primeExp.size() || idxThis < primeExp.size()) {
          if (idxOth >= oth.primeExp.size()) {
            /* exhausted the list in oth.primeExp; copy over the remaining 'this'
             * if multiplying or lcm, discard if gcd.
             */
            if (type == 0 || type == 2) {
              prod.primeExp.add(primeExp.elementAt(idxThis));
              prod.primeExp.add(primeExp.elementAt(idxThis + 1));
            }
            idxThis += 2;
          } else if (idxThis >= primeExp.size()) {
            /* exhausted the list in primeExp; copy over the remaining 'oth'
             */
            if (type == 0 || type == 2) {
              prod.primeExp.add(oth.primeExp.elementAt(idxOth));
              prod.primeExp.add(oth.primeExp.elementAt(idxOth + 1));
            }
            idxOth += 2;
          } else {
            Integer p;
            int ex;
            switch (primeExp.elementAt(idxThis).compareTo(oth.primeExp.elementAt(idxOth))) {
              case 0 -> {
                /* same prime bases p in both factors */
                p = primeExp.elementAt(idxThis);
                ex = switch (type) {
                  case 0 ->
                    /* product means adding exponents */
                      primeExp.elementAt(idxThis + 1) +
                          oth.primeExp.elementAt(idxOth + 1);
                  case 1 ->
                    /* gcd means minimum of exponents */
                      Math.min(
                          primeExp.elementAt(idxThis + 1),
                          oth.primeExp.elementAt(idxOth + 1));
                  default ->
                    /* lcm means maximum of exponents */
                      Math.max(
                          primeExp.elementAt(idxThis + 1),
                          oth.primeExp.elementAt(idxOth + 1));
                };
                prod.primeExp.add(p);
                prod.primeExp.add(ex);
                idxOth += 2;
                idxThis += 2;
              }
              case 1 -> {
                /* this prime base bigger than the other and taken later */
                if (type == 0 || type == 2) {
                  prod.primeExp.add(oth.primeExp.elementAt(idxOth));
                  prod.primeExp.add(oth.primeExp.elementAt(idxOth + 1));
                }
                idxOth += 2;
              }
              default -> {
                /* this prime base smaller than the other and taken now */
                if (type == 0 || type == 2) {
                  prod.primeExp.add(primeExp.elementAt(idxThis));
                  prod.primeExp.add(primeExp.elementAt(idxThis + 1));
                }
                idxThis += 2;
              }
            }
          }
        }
      }
    }
    return prod;
  } /* Ifactor.multiGcdLcm */

  /**
   * Integer division through  another positive integer.
   *
   * @param oth the denominator.
   * @return the division of this through the oth, discarding the remainder.
   * @author Richard J. Mathar
   */
  public Ifactor divide(final Ifactor oth) {
    /* todo: it'd probably be faster to cancel the gcd(this,oth) first in the prime power
     * representation, which would avoid a more strenuous factorization of the integer ratio
     */
    return new Ifactor(n.divide(oth.n));
  } /* Ifactor.divide */

  /**
   * Summation with another positive integer
   *
   * @param oth the other term.
   * @return the sum of both numbers
   * @author Richard J. Mathar
   */
  public Ifactor add(final BigInteger oth) {
    /* avoid re-factorization if oth is zero...
     */
    if (oth.compareTo(BigInteger.ZERO) != 0) {
      return new Ifactor(n.add(oth));
    } else {
      return this;
    }
  } /* Ifactor.add */

  /**
   * Exponentiation with a positive integer.
   *
   * @param exponent the non-negative exponent
   * @return n^exponent. If exponent=0, the result is 1.
   * @author Richard J. Mathar
   */
  public Ifactor pow(final int exponent) throws ArithmeticException {
    /* three simple cases first
     */
    if (exponent < 0) {
      throw new ArithmeticException("Cannot raise " + this + " to negative " + exponent);
    } else if (exponent == 0) {
      return new Ifactor(1);
    } else if (exponent == 1) {
      return this;
    }

    /* general case, the vector with the prime factor powers, which are component-wise
     * exponentiation of the individual prime factor powers.
     */
    Ifactor powers = new Ifactor(0);
    for (int i = 0; i < primeExp.size(); i += 2) {
      Integer p = primeExp.elementAt(i);
      int ex = primeExp.elementAt(i + 1);
      powers.primeExp.add(p);
      powers.primeExp.add(ex * exponent);
    }
    return powers;
  } /* Ifactor.pow */

  /**
   * Pulling the r-th root.
   *
   * @param r the positive or negative (nonzero) root.
   * @return n^(1/r).
   * The return value falls into the Ifactor class if r is positive, but if r is negative
   * a Rational type is needed.
   * @author Richard J. Mathar
   * @since 2009-05-18
   */
  public Rational root(final int r) throws ArithmeticException {
    if (r == 0) {
      throw new ArithmeticException("Cannot pull zeroth root of " + this);
    } else if (r < 0) {
      /* a^(-1/b)= 1/(a^(1/b))
       */
      final Rational invRoot = root(-r);
      return Rational.ONE.divide(invRoot);
    } else {
      BigInteger powers = BigInteger.ONE;
      for (int i = 0; i < primeExp.size(); i += 2) {
        /* all exponents must be multiples of r to succeed (that is, to
         * stay in the range of rational results).
         */
        int ex = primeExp.elementAt(i + 1);
        if (ex % r != 0) {
          throw new ArithmeticException("Cannot pull " + r + "th root of " + this);
        }

        powers.multiply(new BigInteger("" + primeExp.elementAt(i)).pow(ex / r));
      }
      /* convert result to a Rational; unfortunately this will lose the prime factorization */
      return new Rational(powers);
    }
  } /* Ifactor.root */


  /**
   * The set of positive divisors.
   *
   * @return the vector of divisors of the absolute value, sorted.
   * @author Richard J. Mathar
   * @since 2010-08-27
   */
  public Vector<BigInteger> divisors() {
    /* Recursive approach: the divisors of p1^e1*p2^e2*..*py^ey*pz^ez are
     * the divisors that don't contain  the factor pz, and the
     * divisors that contain any power of pz between 1 and up to ez multiplied
     * by 1 or by a product that contains the factors p1..py.
     */
    Vector<BigInteger> d = new Vector<>();
    if (n.compareTo(BigInteger.ZERO) == 0) {
      return d;
    }
    d.add(BigInteger.ONE);
    if (n.compareTo(BigInteger.ONE) > 0) {
      /* Computes sigmaInCompile(p1^e*p2^e2...*py^ey) */
      Ifactor dp = dropPrime();

      /* get ez */
      final int ez = primeExp.lastElement();

      Vector<BigInteger> partD = dp.divisors();

      /* obtain pz by lookup in the prime list */
      final BigInteger pz = new BigInteger(primeExp.elementAt(primeExp.size() - 2).toString());

      /* the output contains all products of the form partD[]*pz^ez, ez>0,
       * and with the exception of the 1, all these are appended.
       */
      for (int i = 1; i < partD.size(); i++) {
        d.add(partD.elementAt(i));
      }
      for (int e = 1; e <= ez; e++) {
        final BigInteger pZez = pz.pow(e);
        for (int i = 0; i < partD.size(); i++) {
          d.add(partD.elementAt(i).multiply(pZez));
        }
      }
    }
    Collections.sort(d);
    return d;
  } /* Ifactor.divisors */

  /**
   * Sum of the divisors of the number.
   *
   * @return the sum of all divisors of the number, 1+....+n.
   * @author Richard J. Mathar
   */
  public Ifactor sigma() {
    return sigma(1);
  } /* Ifactor.sigma */

  /**
   * Sum of the k-th powers of divisors of the number.
   *
   * @param k The exponent of the powers.
   * @return the sum of all divisors of the number, 1^k+....+n^k.
   * @author Richard J. Mathar
   */
  public Ifactor sigma(int k) {
    /* the question is whether keeping a factorization  is worth the effort
     * or whether one should simply multiply these to return a BigInteger...
     */
    if (n.compareTo(BigInteger.ONE) == 0) {
      return ONE;
    } else if (n.compareTo(BigInteger.ZERO) == 0) {
      return ZERO;
    } else {
      /* multiplicative: sigma_k(p^e) = [p^(k*(e+1))-1]/[p^k-1]
       * sigma_0(p^e) = e+1.
       */
      Ifactor resul = Ifactor.ONE;
      for (int i = 0; i < primeExp.size(); i += 2) {
        int ex = primeExp.elementAt(i + 1);
        if (k == 0) {
          resul = resul.multiply(ex + 1);
        } else {
          Integer p = primeExp.elementAt(i);
          BigInteger num = (new BigInteger(p.toString())).pow(k * (ex + 1)).subtract(
              BigInteger.ONE);
          BigInteger den = (new BigInteger(p.toString())).pow(k).subtract(BigInteger.ONE);
          /* This division is of course exact, no remainder
           * The costly prime factorization is hidden here.
           */
          Ifactor f = new Ifactor(num.divide(den));
          resul = resul.multiply(f);
        }
      }
      return resul;
    }
  } /* Ifactor.sigma */

  /**
   * Divide through the highest possible power of the highest prime.
   * If the current number is the prime factor product p1^e1 * p2*e2* p3^e3*...*py^ey * pz^ez,
   * the value returned has the final factor pz^ez eliminated, which gives
   * p1^e1 * p2*e2* p3^e3*...*py^ey.
   *
   * @return the new integer obtained by removing the highest prime power.
   * If this here represents 0 or 1, it is returned without change.
   * @author Richard J. Mathar
   * @since 2006-08-20
   */
  public Ifactor dropPrime() {
    /* the cases n==1 or n ==0
     */
    if (n.compareTo(BigInteger.ONE) <= 0) {
      return this;
    }

    /* The cases n>1
     * Start empty. Copy all but the last factor over to the result
     * the vector with the new prime factor powers, which contain the
     * old prime factor powers up to but not including the last one.
     */
    Ifactor pow = new Ifactor(0);
    pow.n = BigInteger.ONE;
    for (int i = 0; i < primeExp.size() - 2; i += 2) {
      pow.primeExp.add(primeExp.elementAt(i));
      pow.primeExp.add(primeExp.elementAt(i + 1));
      BigInteger p = new BigInteger(primeExp.elementAt(i).toString());
      int ex = primeExp.elementAt(i + 1);
      pow.n = pow.n.multiply(p.pow(ex));
    }
    return pow;
  } /* Ifactor.dropPrime */

  /**
   * Test whether this is a square of an integer (perfect square).
   *
   * @return true if this is an integer squared (including 0), else false
   * @author Richard J. Mathar
   */
  public boolean isSquare() {
    /* check the exponents, located at the odd-indexed positions
     */
    for (int i = 1; i < primeExp.size(); i += 2) {
      if (primeExp.elementAt(i) % 2 != 0) {
        return false;
      }
    }
    return true;
  } /* Ifactor.isSquare */

  /**
   * The sum of the prime factor exponents, with multiplicity.
   *
   * @return the sum over the primeExp numbers
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public int bigOmega() {
    int result = 0;
    for (int i = 1; i < primeExp.size(); i += 2) {
      result += primeExp.elementAt(i);
    }
    return (result);
  } /* Ifactor.bigOmega */

  /**
   * The sum of the prime factor exponents, without multiplicity.
   *
   * @return the number of distinct prime factors.
   * @author Richard J. Mathar
   * @since 2008-10-16
   */
  @SuppressWarnings("unused")
  public int omega() {
    return primeExp.size() / 2;
  } /* Ifactor.omega */

  /**
   * The square-free part.
   *
   * @return the minimum m such that m times this number is a square.
   * @author Richard J. Mathar
   * @since 2008-10-16
   */
  public BigInteger core() {
    BigInteger result = BigInteger.ONE;
    for (int i = 0; i < primeExp.size(); i += 2) {
      if (primeExp.elementAt(i + 1) % 2 != 0) {
        result = result.multiply(new BigInteger(primeExp.elementAt(i).toString()));
      }
    }
    return result;
  } /* Ifactor.core */

  /**
   * The Mobius function.
   * 1 if n=1, else, if k is the number of distinct prime factors, return (-1)^k,
   * else, if k has repeated prime factors, return 0.
   *
   * @return the Mobius function.
   * @author Richard J. Mathar
   */
  public int mobius() {
    if (n.compareTo(BigInteger.ONE) <= 0) {
      return 1;
    }
    /* accumulate number of different primes in k */
    int k = 1;
    for (int i = 0; i < primeExp.size(); i += 2) {
      final int e = primeExp.elementAt(i + 1);
      if (e > 1) {
        return 0;
      } else if (e == 1)
        /* accumulates (-1)^k */ {
        k *= -1;
      }

    }
    return (k);
  } /* Ifactor.mobius */

  /**
   * Maximum of two values.
   *
   * @param oth the number to compare this with.
   * @return the larger of the two values.
   * @author Richard J. Mathar
   */
  public Ifactor max(final Ifactor oth) {
    if (n.compareTo(oth.n) >= 0) {
      return this;
    } else {
      return oth;
    }
  } /* Ifactor.max */

  /**
   * Minimum of two values.
   *
   * @param oth the number to compare this with.
   * @return the smaller of the two values.
   * @author Richard J. Mathar
   */
  public Ifactor min(final Ifactor oth) {
    if (n.compareTo(oth.n) <= 0) {
      return this;
    } else {
      return oth;
    }
  } /* Ifactor.min */

  /**
   * Maximum of a list of values.
   *
   * @param set list of numbers.
   * @return the largest in the list.
   * @author Richard J. Mathar
   */
  public static Ifactor max(final Vector<Ifactor> set) {
    Ifactor result = set.elementAt(0);
    for (int i = 1; i < set.size(); i++) {
      result = result.max(set.elementAt(i));
    }
    return result;
  } /* Ifactor.max */

  public static Ifactor max(final Collection<Ifactor> collection) {
    return collection.stream().max(Ifactor::compareTo).orElseThrow();
  } /* Ifactor.max */

  @SuppressWarnings("unused")
  public static Ifactor min(final Collection<Ifactor> collection) {
    return collection.stream().min(Ifactor::compareTo).orElseThrow();
  } /* Ifactor.max */

  /**
   * Minimum of a list of values.
   *
   * @param set list of numbers.
   * @return the smallest in the list.
   * @author Richard J. Mathar
   */
  @SuppressWarnings("unused")
  public static Ifactor min(final Vector<Ifactor> set) {
    Ifactor result = set.elementAt(0);
    for (int i = 1; i < set.size(); i++) {
      result = result.min(set.elementAt(i));
    }
    return result;
  } /* Ifactor.min */

  /**
   * Compare value against another Ifactor
   *
   * @param oth The value to be compared again.
   * @return 1, 0 or -1 according to being larger, equal to or smaller than oth.
   * @author Richard J. Mathar
   * @since 2012-02-15
   */
  public int compareTo(final Ifactor oth) {
    return n.compareTo(oth.n);
  } /* compareTo */

  /**
   * Convert to printable format
   *
   * @return a string of the form n:prime^pow*prime^pow*prime^pow...
   * @author Richard J. Mathar
   */
  public String toString() {
    StringBuilder result = new StringBuilder(n.toString() + ":");
    if (n.compareTo(BigInteger.ONE) == 0) {
      result.append("1");
    } else {
      boolean firstMul = true;
      for (int i = 0; i < primeExp.size(); i += 2) {
        if (!firstMul) {
          result.append("*");
        }
        if (primeExp.elementAt(i + 1) > 1) {
          result.append(primeExp.elementAt(i).toString()).append("^").append(primeExp.elementAt(i + 1).toString());
        } else {
          result.append(primeExp.elementAt(i).toString());
        }
        firstMul = false;
      }
    }
    return result.toString();
  } /* Ifactor.toString */

  /**
   * Test program.
   * It takes a single argument n and prints the integer factorization.<br>
   * java -cp . org.neveC.rjm.Ifactor n<br>
   *
   * @param args It takes a single argument n and prints the integer factorization.<br>
   * @author Richard J. Mathar
   */
  public static void main(String[] args) {
    BigInteger n = new BigInteger("135385569");
    System.out.println(new Ifactor(n));
  } /* Ifactor.main */
} /* Ifactor */
