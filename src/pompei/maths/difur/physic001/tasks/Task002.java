package pompei.maths.difur.physic001.tasks;

import pompei.maths.difur.Stepper_H4_Hoine;
import pompei.maths.difur.Stepper_H5_RungeKutta;
import pompei.maths.difur.physic001.ball.Ball;
import pompei.maths.difur.physic001.ball.Fix;
import pompei.maths.difur.physic001.ball.Rubber;
import pompei.maths.difur.physic001.ball.Task;
import pompei.maths.difur.physic001.movie.Movie;

import java.nio.file.Paths;

public class Task002 {
  public static void main(String[] args) {

    Fix fix = new Fix();
    fix.x = 7;
    fix.y = 10;

    Ball ball = new Ball();
    ball.x = 13;
    ball.y = 3;
    ball.mas = 3;
    ball.radius = 0.5;

    Rubber rubber = new Rubber();
    rubber.K = 400;
    rubber.L = 7;

    rubber.connect1(fix);
    rubber.connect2(ball);

    Task task = new Task();
    task.registerBall(ball);
    task.registerFigure(fix);
    task.registerFigure(rubber);

    task.movie = new Movie(800, 600, 0, 0, 20, 15, Paths.get("build/task002/png_list"));

    task.t1 = 0;
    task.t2 = 40;
    task.dt = 0.0001;

    task.stepper = new Stepper_H5_RungeKutta();

    task.go();
  }
}
