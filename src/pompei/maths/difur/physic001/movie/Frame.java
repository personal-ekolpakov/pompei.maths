package pompei.maths.difur.physic001.movie;

import lombok.SneakyThrows;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.file.Path;

public class Frame implements AutoCloseable {
  public final Path outFile;
  private final int width;
  private final int height;
  private final double x1;
  private final double y1;
  private final double x2;
  private final double y2;
  private final BufferedImage bufferedImage;
  private final Graphics2D g;

  public Frame(Path outFile, int width, int height, double x1, double y1, double x2, double y2) {
    this.outFile = outFile;
    this.width = width;
    this.height = height;
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;

    bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    g = bufferedImage.createGraphics();
    g.setColor(new Color(255, 255, 255));
    g.fillRect(0, 0, width, height);
  }

  @Override
  @SneakyThrows
  public void close() {
    g.dispose();
    outFile.toFile().getParentFile().mkdirs();
    ImageIO.write(bufferedImage, "png", outFile.toFile());
  }

  public void setColor(Color color) {
    g.setColor(color);
  }

  public int toIntX(double x) {
    return (int) Math.round((x - x1) / (x2 - x1) * width);
  }

  public int toIntY(double y) {
    return (int) Math.round((y2 - y) / (y2 - y1) * height);
  }

  public void fillCircle(double x, double y, double radius) {

    int Radius = toIntX(radius);
    int X = toIntX(x);
    int Y = toIntY(y);

    g.fillOval(X - Radius, Y - Radius, Radius * 2, Radius * 2);

  }

  public void line(double x1, double y1, double x2, double y2) {
    int X1 = toIntX(x1);
    int Y1 = toIntY(y1);
    int X2 = toIntX(x2);
    int Y2 = toIntY(y2);
    g.drawLine(X1, Y1, X2, Y2);
  }

  public void drawText(int X, int Y, String text) {
    g.drawString(text, X, Y);
  }

  public void lineIntBased(double x, double y, int dX1, int dY1, int dX2, int dY2) {
    int X = toIntX(x);
    int Y = toIntY(y);
    g.drawLine(X + dX1, Y + dY1, X + dX2, Y + dY2);
  }

  public void drawIntCircleBased(double x, double y, int dX, int dY, int radius) {
    int X = toIntX(x) + dX;
    int Y = toIntY(y) + dY;

    g.drawOval(X - radius, Y - radius, radius * 2, radius * 2);
  }
}
