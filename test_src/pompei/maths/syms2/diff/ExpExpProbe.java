package pompei.maths.syms2.diff;

import pompei.maths.syms_diff.model.Form;
import pompei.maths.syms_diff.visitable.frm;
import pompei.maths.syms_diff.visitors.KillDivVisitor;
import pompei.maths.syms_diff.visitors.paint.PaintUtil;

import java.io.IOException;

import static pompei.maths.syms2.diff.DiffVisitorProbe.openBracketsAndSimilar;
import static pompei.maths.syms_diff.visitable.frm.f;
import static pompei.maths.syms_diff.visitable.frm.p;

public class ExpExpProbe {
  public static void main(String[] args) throws IOException {

    Form x = f("x");

    Form exp = exp(x, 4);

    PaintUtil.paintToFile("build/ExpExpProbe.png",
                          exp.visit(R.S));
  }

  private static Form exp(Form x, int n) {

    Form sum = f("1");
    Form fact = f("1");

    for (int i = 1; i <= n; i++) {

      sum = f(sum, "+", f(p(x, i), "/", fact));

      fact = f(fact, "*", "" + (i+1));

    }

    return openBracketsAndSimilar(sum.visit(new KillDivVisitor()));
  }
}
