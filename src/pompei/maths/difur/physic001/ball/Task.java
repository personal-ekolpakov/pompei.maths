package pompei.maths.difur.physic001.ball;

import pompei.maths.difur.F;
import pompei.maths.difur.Stepper;
import pompei.maths.difur.physic001.movie.Frame;
import pompei.maths.difur.physic001.movie.Movie;
import pompei.maths.utils.Vec2;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Task {

  private final List<Ball> balls = new ArrayList<>();
  private final List<Figure> figures = new ArrayList<>();
  public int t1;
  public int t2;
  public double dt;
  public Movie movie;
  public Stepper stepper;

  public void registerBall(Ball ball) {
    balls.add(ball);
    figures.add(ball);
  }

  public void registerFigure(Figure figure) {
    figures.add(figure);
  }

  private void saveFromBalls(double[] x) {

    for (Ball ball : balls) {
      int i0 = ball.index * 4;
      int i1 = i0 + 1;
      int i2 = i0 + 2;
      int i3 = i0 + 3;

      x[i0] = ball.x;
      x[i1] = ball.y;
      x[i2] = ball.vx;
      x[i3] = ball.vy;
    }

  }

  private void loadToBalls(double[] x) {

    for (Ball ball : balls) {
      int i0 = ball.index * 4;
      int i1 = i0 + 1;
      int i2 = i0 + 2;
      int i3 = i0 + 3;

      ball.x = x[i0];
      ball.y = x[i1];
      ball.vx = x[i2];
      ball.vy = x[i3];
    }

  }

  public void go() {

    for (int i = 0; i < balls.size(); i++) {
      balls.get(i).index = i;
    }

    int N = balls.size() * 4;

    stepper.prepare(N);

    saveFromBalls(stepper.getX());

    F F = (double[] res, double t, double[] x) -> {

      loadToBalls(x);

      for (Ball ball : balls) {

        int i0 = ball.index * 4;
        int i1 = i0 + 1;
        int i2 = i0 + 2;
        int i3 = i0 + 3;

        Vec2 Force = ball.Force();
        double mas = ball.mas;

        res[i0] = x[i2];
        res[i1] = x[i3];
        res[i2] = Force.x / mas;
        res[i3] = Force.y / mas - 9.8;
      }

    };

    double nextFrameT = t1;

    Movie movie = this.movie;

    for (double t = t1; t <= t2; t += dt) {

      if (movie != null && nextFrameT <= t) {
        nextFrameT += 1.0 / 24;

        try (Frame frame = movie.createFrame()) {
          figures.forEach(f -> f.paint(frame));

          frame.setColor(new Color(0, 0, 0));
          frame.drawText(movie.width / 2, 50, "t = " + t);

          System.out.println("rSu1qzU3oh :: FRAME " + frame.outFile + ", t = " + t);
        }

      }

      stepper.step(F, t, dt, N);

    }
  }
}
