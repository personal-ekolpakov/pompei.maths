package pompei.maths.difur.physic001.ball;

import pompei.maths.utils.Vec2;

public class AirFriction {
  public double k1;
  public double k2;

  public void connect(Dot dot) {
    dot.addForceSource(() -> {

      double vx = dot.getVX();
      double vy = dot.getVY();

      Vec2 v = Vec2.xy(vx, vy);

      double vv = v.length();

      if (vv < 1e-6) {
        return Vec2.xy(0, 0);
      }

      double F = vv * k1 + vv * vv * k2;

      return v.div(vv).mul(-F);
    });
  }
}
