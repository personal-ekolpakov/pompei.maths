package pompei.maths.password_hash;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import pompei.maths.utils.Conv;

import static java.nio.charset.StandardCharsets.UTF_8;

public class HashPassword {
  public static void main(String[] args) {
    System.out.println("0d00i6G1pa");

    for (int i = 0; i < 1000; i++) {
      doIt(4, 128 * 128, 8);
      doIt(8, 128 * 128, 10);
    }

  }

  @SuppressWarnings("SameParameterValue")
  private static void doIt(int iterations, int memory, int parallelism) {
    Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);

    var time1 = System.nanoTime();
    String hash = argon2.hash(iterations, memory, parallelism, "status12".getBytes(UTF_8));
    var time2 = System.nanoTime();
    var verify = argon2.verify(hash, "status12".getBytes(UTF_8));
    var time3 = System.nanoTime();

    System.out.println("iUL1BHUEjL ::   hash = " + hash);
    System.out.println("iUL1BHUEjL :: verify = " + verify);
    var generateTime = Conv.nanoToSec(time1, time2);
    var verifyTime = Conv.nanoToSec(time2, time3);

    System.out.println("wd1hjeT20o :: generateTime = " + generateTime);
    System.out.println("wd1hjeT20o ::   verifyTime = " + verifyTime);
  }
}
